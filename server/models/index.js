const pool = require("../db/");

class Employees{

	static _queryDb(sql, props){
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
				}

				connection.query(sql, props, (err, rows) => {
					if ( err ) {
						reject(err);
					} else {
						resolve(rows);
					}
				});

				connection.on("error", function(err){
					reject(err);
				});

				connection.release();
			});
		});
	}

	static addEmployee(data){
		return this._queryDb("INSERT INTO employees SET ?", data);
	}

	static getAllEmployees(){
		return this._queryDb("SELECT * FROM employees", null);
	}

	static updateEmployee(data, id){
		return this._queryDb("UPDATE employees SET ? WHERE id = ?", [data, id]);
	}

	static deleteEmployee(id){
		return this._queryDb("DELETE FROM employees WHERE id = ?", [id]);
	}
}

module.exports = Employees;

