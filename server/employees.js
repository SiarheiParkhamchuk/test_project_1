function checkEmptyField(obj){
	for ( let key in obj ){
		if ( !obj.hasOwnProperty(key) ) continue;
		if ( obj[key].trim() === "" ) return false;
	}
	return true;
}

function queryDb(sql, props){
	return new Promise((resolve, reject) => {
		pool.getConnection((err, connection) => {
			if (err) reject(err);

			connection.query(sql, props, (err, rows) => {
				if ( err ) reject(err);
				else resolve(rows);
			});

			connection.on("error", function(err){
				reject(err);
			});

			connection.release();
		});
	});
}

module.exports = function(app, dbTable ){
	app.options("/*", function(req, res){
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
		res.sendStatus(200);
	});

	app.post("/", function(req, res){
		queryDb( `INSERT INTO ${dbTable} SET ?`,  req.body )
			.then( rows => { res.json({ "id":rows.insertId }); })
			.catch( error => { throw error; });
	});

	app.get("/", function(req, res){
		queryDb( `SELECT * FROM ${dbTable}`, null )
			.then( rows => { res.json(rows); })
			.catch( error => { throw error; });
	});

	app.put("/:id", function(req, res){
		if ( !checkEmptyField(req.body) ) {
			res.status(500).send("Something was broken");
			return false;
		}

		queryDb( `UPDATE ${dbTable} SET ? WHERE id = ?`, [req.body, req.params.id] )
			.then( () => { res.json(req.params); })
			.catch( error => { throw error; });
	});

	app.delete("/:id", function(req, res){
		queryDb( `DELETE FROM ${dbTable} WHERE id = ?`, req.params.id )
			.then( () => { res.json(req.params); })
			.catch( error => { throw error; });
	});
};