const env = require("optimist").argv.env;
const _ = require("lodash");
const def = require("./default");

const config = require("./" + (env || "local"));

module.exports = _.merge({}, def, config);