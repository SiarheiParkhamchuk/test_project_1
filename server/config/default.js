const config = {
	app: {
		port: 3000,
		host:"localhost"
	},
	db:{
		host: "127.0.0.1",
		user:"root",
		password:"12345",
		database:"employees",
		pool:{
			connectionLimit: 10
		}
	}
};

module.exports = config;