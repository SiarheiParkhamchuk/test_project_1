const express = require("express");
const config = require("./config/");

const app  = express();
require("./middleware/index")(app);
require("./routs/index")(app);


app.listen(config.app.port, config.app.host);