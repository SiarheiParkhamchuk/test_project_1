const express = require("express");

module.exports = function(app){
	app.use(express.urlencoded({extended: true}));
	app.use(express.json());
	app.use((req, res, next) => {
		res.set({
			"Access-Control-Allow-Origin" : "*",
			"Content-Type" : "application/json"
		});
		res.status(200);
		next();
	});
};