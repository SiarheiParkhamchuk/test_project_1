const employeesController = require("../controllers/");

module.exports = function(app){
	app.options("/*", (req, res) => {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
		res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
		res.sendStatus(200);
	});

	app.post("/", (req, res) => {
		employeesController.addEmployee(req.body)
			.then( rows => { res.json({ "id":rows.insertId }); })
			.catch( error => { throw error; });
	});

	app.get("/", (req, res) => {
		employeesController.getAllEmployees()
			.then( rows => { res.json(rows); })
			.catch( error => { throw error; });
	});

	app.put("/:id", (req, res) => {
		employeesController.updateEmployee(req.body, req.param.id)
			.then( () => { res.json(req.params); })
			.catch( error => { throw error; });
	});

	app.delete("/:id", (req, res) => {
		employeesController.deleteEmployee(req.params.id)
			.then( () => { res.json(req.params); })
			.catch( error => { throw error; });
	});
};