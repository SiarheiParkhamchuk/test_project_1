const mysql = require("mysql");
const configDb = require("../config/").db;

module.exports = mysql.createPool({
	connectionLimit : configDb.pool.connectionLimit,
	host            : configDb.host,
	user            : configDb.user,
	password        : configDb.password,
	database        : configDb.database
});