const Employees = require("../models/");

function checkEmptyField(obj){
	for ( let key in obj ){
		if ( !obj.hasOwnProperty(key) ) continue;
		if ( obj[key].trim() === "" ) return false;
	}
	return true;
}

exports.addEmployee = function(body){
	return Employees.addEmployee(body);
};

exports.getAllEmployees = function(){
	return Employees.getAllEmployees();
};

exports.updateEmployee = function(body, id){
	if ( !checkEmptyField(body) ) {
		return new Error("Fields must be filled");
	}

	return Employees.updateEmployee(body, id);
};

exports.deleteEmployee = function(id){
	return Employees.deleteEmployee(id);
};