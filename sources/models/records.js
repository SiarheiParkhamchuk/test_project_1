export const employees = new webix.DataCollection({
	url:"http://localhost:8000",
	save:"rest->http://localhost:8000",

	scheme:{
		$init:function(obj){
			let birth = webix.Date.strToDate("%d %M %Y");
			obj.Birth = birth(obj.Birth);
		},

		$save:function(obj){
			let birth = webix.Date.dateToStr("%d %M %Y");
			obj.Birth = birth(obj.Birth);
		},
	},
});