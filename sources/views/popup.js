import {JetView} from "webix-jet";
import {employees} from "models/records";

export default class PopupEmployeesView extends JetView {
	config(){
		let form = {
			view:"form",
			localId:"popupForm",
			width: 400,
			elements:[
				{
					view:"text",
					name:"FirstName",
					label:"First name",
					labelWidth:100,
					required:true,
					invalidMessage:"Field must be filling"
				},
				{
					view:"text",
					name:"LastName",
					label:"Last name",
					labelWidth:100,
					required:true,
					invalidMessage:"Field must be filling"
				},
				{
					view:"datepicker",
					name:"Birth",
					label:"Birthday",
					labelWidth:100,
					required:true,
					format:"%d.%m.%Y",
					invalidMessage:"Field must be filling"
				},
				{
					view:"text",
					name:"Salary",
					label:"Salary",
					labelWidth:100,
					required:true,
					invalidMessage:"Field must be filling and number"
				},
				{
					cols:[
						{
							view:"button",
							localId:"add",
							value:"Add",
						},
						{
							view:"button",
							localId:"cancel",
							value:"Cancel",
						}
					]
				}

			],
			rules:{
				FirstName:webix.rules.isNotEmpty,
				LastName:webix.rules.isNotEmpty,
				Birth:webix.rules.isNotEmpty,
				Salary:webix.rules.isNotEmpty && webix.rules.isNumber
			}
		};

		return {
			view:"window",
			localId:"employeePopup",
			minHeight:300,
			autofit:true,
			head:"Add",
			position:"center",
			modal:"true",
			body:form
		};
	}

	showPopup(settings){
		if ( settings.header ) this.getRoot().getHead().setHTML(settings.header);
		if ( settings.label ) this.$$("add").setValue(settings.label);

		if ( settings.id ) {
			let values = employees.getItem(settings.id);
			this.$$("popupForm").setValues(values);
		}

		this.getRoot().show();
	}

	init(view){
		let popupForm = this.$$("popupForm"),
			addBtn = this.$$("add"),
			cancelBtn = this.$$("cancel");

		this.on(addBtn, "onItemClick", () => {
			if ( !popupForm.validate() ) return false;
			let values = popupForm.getValues();

			if ( values.id )
				employees.updateItem(values.id, values);
			else{
				employees.add(values);
			}

			view.hide();
		});

		this.on(cancelBtn, "onItemClick", () => {
			this.getRoot().hide();
		});

		this.on(view, "onHide", () => {
			popupForm.clear();
			popupForm.clearValidation();
		});
	}
}