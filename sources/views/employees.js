import {JetView} from "webix-jet";
import TableView from "views/table";

export default class TopView extends JetView{
	config(){
		let header = {
			view:"template",
			type:"header",
			template:"Employees"
		};

		return {
			rows:[
				header,
				TableView
			]
		};
	}
	init(){

	}
}