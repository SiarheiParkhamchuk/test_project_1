import {JetView} from "webix-jet";
import PopupEmployeesView from "views/popup";
import {employees} from "models/records";

export default class TableView extends JetView {
	config(){
		let cols = [
			{
				id:"FirstName",
				header:"First name",
				fillspace:2,
				sort:"text",
				editor:"text"
			},
			{
				id:"LastName",
				header:"Last name",
				fillspace:2,
				sort:"text",
				editor:"text"
			},
			{
				id:"Birth",
				header:"Birthday",
				fillspace:1,
				format: webix.Date.dateToStr("%d %M %Y"),
				sort:"date",
				editor:"date"
			},
			{
				id:"Salary",
				header:"Salary",
				fillspace:1,
				sort:this.sortBySalary,
				editor:"text",
				format:(value) => {
					return value + " RUB";
				},
				editFormat:(value) => {
					return webix.Number.format(value, {
						groupDelimiter:"",
						groupSize:0,
						decimalDelimiter:"",
						decimalSize:0
					});
				},
			}
		];

		let toolbar = {
			view:"toolbar",
			localId:"employeesToolbar",
			elements:[
				{
					view:"button",
					label:"Add",
					localId:"addEmployee",
					maxWidth:350,
				},
				{
					view:"button",
					label:"Edit",
					localId:"editEmployee",
					maxWidth:350
				},
				{
					view:"button",
					label:"Remove",
					localId:"removeEmployee",
					maxWidth:350
				}
			]
		};

		return {
			rows:[
				{
					view: "datatable",
					localId:"employeesTable",
					scrollX: false,
					select:true,
					editable:true,
					editaction:"dblclick",
					columns:cols,
					rules:{
						Salary:function(value) {
							return webix.rules.isNumber(value) && webix.rules.isNotEmpty(value);
						},
						FirstName:webix.rules.isNotEmpty,
						LastName:webix.rules.isNotEmpty,
						Birth:webix.rules.isNotEmpty,
					},
					on:{
						onValidationError:(id, obj, details) => {

							for ( let key in details ){
								if ( !details.hasOwnProperty(key) ) continue;

								let message = "Field " + key + " must be filled.";
								if ( key === "Salary" )
									message = "Field " + key + " must be number and not empty.";

								webix.message({
									text:message,
									type:"error"
								});
							}
						},
					}
				},
				toolbar
			]

		};
	}

	sortBySalary(a, b) {
		a = parseInt(a.Salary);
		b = parseInt(b.Salary);
		return a>b ? 1 :(a<b?-1:0);
	}

	selectItem(view, collection, param){
		let id = this.getParam(param) || view.getFirstId();

		if ( id && collection.exists(id) && id !== view.getSelectedId() ){
			view.select(id);
			this.app.webix.UIManager.setFocus(view);
		}
	}

	removeItem(id, collection, view){
		let prevId = view.getNextId(id) || view.getPrevId(id);

		webix.confirm({
			title:"Delete",
			ok:"Yep",
			cancel:"Nope",
			text:"Delete this employee?",
			callback:function (res) {
				if ( res ) {
					collection.remove(id);
					if ( !prevId ) {
						view.showOverlay("Empty");
						return false;
					}
					view.select(prevId);
				}
			}
		});
	}

	init(){
		let employeesTable = this.$$("employeesTable"),
			addEmployee = this.$$("addEmployee"),
			editEmployee = this.$$("editEmployee"),
			removeEmployee = this.$$("removeEmployee"),
			dpEmployees = webix.dp(employees);

		employeesTable.sync(employees);

		employees.waitData.then(() => {
			this.selectItem(employeesTable, employees, "id");
		});

		this.popup = this.ui(PopupEmployeesView);

		this.on(dpEmployees, "onAfterSave", (res) => {
			if ( !employeesTable.isSelected(res.id) && employees.exists(res.id) ) {
				employeesTable.select(res.id);
			}
			if ( employeesTable.count() == 1 )  employeesTable.hideOverlay();

			this.app.webix.UIManager.setFocus(employeesTable);
		});

		this.on(addEmployee, "onItemClick", () => {
			this.popup.showPopup({
				header:"Add employee",
				label:"Add"
			});
		});

		this.on(editEmployee, "onItemClick", () => {
			this.popup.showPopup({
				header:"Edit employee",
				label:"Update",
				id:this.getParam("id")
			});
		});

		this.on(removeEmployee, "onItemClick", () => {
			let id = this.getParam("id");
			this.removeItem(id, employees, employeesTable);
		});

		this.on(employeesTable, "onAfterSelect", (obj) => {
			this.setParam("id", obj.id, true );
		});

		this.on(employeesTable, "onKeyPress", (code, e) =>  {
			if ( code === 46 && !e.ctrlKey && !e.shiftKey && !e.altKey ) {
				let id = this.getParam("id");
				if ( id ) {
					this.removeItem(id, employees, employeesTable);
				}
			}
		});
	}
}