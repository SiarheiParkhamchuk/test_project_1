import "./styles/app.css";
import {JetApp, EmptyRouter, HashRouter } from "webix-jet";

export default class Task_01 extends JetApp{
	constructor(config){
		const defaults = {
			id 		: "TASK-01",
			version : "1.0.0",
			router 	: BUILD_AS_MODULE ? EmptyRouter : HashRouter,
			debug 	: true,
			start 	: "/employees"
		};

		super({ ...defaults, ...config });
	}
}

if (!BUILD_AS_MODULE){
	webix.ready(() => new Task_01().render() );
}